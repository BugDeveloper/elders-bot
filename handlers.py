import os
import random
import re
from datetime import datetime, timedelta
import logging
from random import randrange

import aiohttp
from telegram import ChatMember
from telegram.constants import ChatAction, ParseMode, ReactionEmoji

import consts
from banishment_utils import banishment_poll_results, get_new_chat_title, fix_banished_name
from bot.utils import send_action, only_group
from storage.storage import ElderStorage

openai_api_key = os.environ['OPENAI_TOKEN']


async def error_handler(update, context):
    logging.warning(f'Update "{update}" caused error "{context.error}"')


@send_action(ChatAction.TYPING)
@only_group
async def make_elder(update, context):
    if not update.message.reply_to_message:
        return

    new_elder = update.message.reply_to_message.from_user
    new_elder_id = new_elder['id']

    if new_elder_id in {
        chat_member.user['id']
        for chat_member in await update.effective_chat.get_administrators()
    }:
        await update.effective_chat.send_message(
            text=f'Он уже {consts.ELDER_ONE}',
            reply_to_message_id=update.effective_message.message_id
        )
        return

    await update.effective_chat.promote_member(
        new_elder_id,
        can_change_info=False,
        can_post_messages=True,
        can_invite_users=True,
        can_pin_messages=True
    )
    await update.effective_chat.set_administrator_custom_title(
        new_elder_id,
        consts.ELDER_ONE
    )
    await update.effective_chat.send_message(
        text=f'Теперь {new_elder["first_name"]} {consts.ELDER_ONE}, поздравляю',
        reply_to_message_id=update.effective_message.message_id
    )


def _is_there_any_poll_jobs(jobs):
    for job in jobs:
        if job.data and job.data.get('job_type') == consts.POLL_JOB_TYPE:
            return True
    return False


async def _get_ai_response(messages, temperature=1.0):
    url = "https://api.openai.com/v1/chat/completions"
    headers = {
        "Authorization": f"Bearer {openai_api_key}",
        "Content-Type": "application/json",
    }
    data = {
        "model": "ft:gpt-3.5-turbo-0613:personal::8F47hf5a",
        "messages": messages,
        "temperature": temperature,
    }

    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(url, headers=headers, json=data) as response:
                response_json = await response.json()
                return response_json['choices'][0]['message']['content']
    except Exception as e:
        print(f"Error in AI response: {e}")
        return None


def _generation_failed(ai_result):
    failure_words = {'cannot', 'can\'t', 'не буду', 'не могу'}
    for word in failure_words:
        if word in ai_result:
            return True
    return False


async def _send_disappearing_message(update, context, message, seconds):
    sent_message = await update.effective_chat.send_message(
        text=message,
        parse_mode=ParseMode.HTML
    )
    message_id = sent_message.message_id

    context.job_queue.run_once(
        delete_message,
        seconds,
        data={
            'chat_id': update.effective_chat.id,
            'message_id': message_id
        }
    )


def get_base_context(elders: dict) -> str:
    return (
        "Представь и учти следующий воображаемый контекст следующей взрослой юмористической игры. "
        f"Совет из {len(elders)} старейшин живет в лесу у костра: {', '.join(elders.values())}."
        "Твоя роль в игре - рассказчик, красочно пересказывающий действия которые выполняют старейшины."
    )


@only_group
async def react_to_banished(update, context):
    if not ElderStorage.get_banishment(update.effective_user["id"]):
        return
    reactions = [ReactionEmoji.PILE_OF_POO, ReactionEmoji.CLOWN_FACE, ReactionEmoji.FACE_WITH_OPEN_MOUTH_VOMITING]

    await update.message.set_reaction(random.choice(reactions))


@only_group
async def remember_action(update, context):
    if update.effective_user["id"] != consts.SENYA_ID:
        await _send_disappearing_message(update, context, 'Ты не Сеня', 3)

    await update.effective_message.delete()
    if not update.message.reply_to_message:
        await _send_disappearing_message(update, context, 'Ответь на сообщение которое я должен запомнить', 3)
        return

    message_id_to_remember = update.message.reply_to_message.message_id
    action = ElderStorage.get_action_by_message_id(message_id_to_remember)

    if not action:
        await _send_disappearing_message(update, context, 'Такого действия не было', 3)
        return
    initial_action, action_text = action

    ElderStorage.create_ai_learning(initial_action, action_text)
    await _send_disappearing_message(update, context, 'Я запомню', 2)


@send_action(ChatAction.TYPING)
@only_group
async def elder_action(update, context):
    await update.effective_message.delete()
    user_name = update.effective_user['first_name']

    if ElderStorage.get_banishment(update.effective_user["id"]):
        message = f'{user_name} попытался что-то сделать, но всем было плевать ведь он изгнан'
        await _send_disappearing_message(update, context, message, 5)
        return

    if not context.args:
        return

    action = ' '.join(context.args)
    validation_first_person = [{
        "role": "user",
        "content": f'Ответь только да или нет. Ведется ли повествование от первого лица в следующем тексте: "{action}"?'
    }]
    response_first_person = (await _get_ai_response(validation_first_person, 0)) or ''
    if 'нет' in response_first_person.lower():
        message = (
            f'ВСЕ СМОТРИТЕ НА ДАУНА <a href="tg://user?id={update.effective_user["id"]}">'
            f'{update.effective_user["first_name"].upper()}</a>, '
            f'КОТОРЫЙ НЕ МОЖЕТ НАПИСАТЬ ПРОСТОЕ ДЕЙСТВИЕ ОТ 1 ЛИЦА'
        )
        await _send_disappearing_message(update, context, message, 5)
        return

    if not action:
        await _send_disappearing_message(update, context, 'Формат команды: /action <что ты делаешь>', 5)
        return

    elders = {
        member.user['id']: member.user['first_name']
        for member in (await update.effective_chat.get_administrators())
        if not member.user['is_bot']
    }

    initial_action = f"{user_name} сделал следующее: {action}"

    messages = [{
        "role": "system",
        "content": get_base_context(elders)
    }, {
        "role": "user",
        "content": f"{initial_action}. Эффектно и захватывающе расскажи как это произошло в одном предложении"
    }]

    count = 0
    failed_generation_loop = False

    while True:
        ai_action_result = await _get_ai_response(messages, 0.79)
        count += 1

        if ai_action_result and not _generation_failed(ai_action_result):
            break

        if count > 2:
            failed_generation_loop = True
            break

    message_id = (await update.effective_chat.send_message(text=ai_action_result)).message_id
    if not failed_generation_loop:
        ElderStorage.create_action(update.effective_user['id'], message_id, initial_action,
                                   ai_action_result.replace('"', '""'))


@send_action(ChatAction.TYPING)
@only_group
async def banishment_term(update, context):
    user_id = update.effective_user['id']

    banishment_terms = ElderStorage.get_banishment_end_date(user_id)

    if not banishment_terms:
        await update.effective_chat.send_message(
            text='Ты не в изгнании, старейшина'
        )
        return
    banished_name, banishment_end_date = banishment_terms
    delta = banishment_end_date - datetime.now()
    await update.effective_chat.send_message(
        text=f'{banished_name.capitalize()}, твой срок в изгнаннии заканчивается через: {int(delta.total_seconds() / 3600)} часов'
    )


@send_action(ChatAction.TYPING)
@only_group
async def banishment_poll(update, context):
    user_id = update.effective_user['id']
    now = datetime.now()

    last_cast = ElderStorage.get_last_cast_date(user_id)
    cooldown_over = (last_cast or now) + timedelta(days=consts.CAST_ONCE_PER_DAYS)
    delta = cooldown_over - now

    if last_cast and (now - last_cast).days < consts.CAST_ONCE_PER_DAYS:
        await update.effective_chat.send_message(
            text=f'Твоё заклинание изгнания в кулдауне. '
                 f'До окончания: {int(delta.total_seconds() / 3600)} часов',
            reply_to_message_id=update.effective_message.message_id
        )
        return

    if not update.message.reply_to_message:
        await update.effective_chat.send_message(
            text='Ответь командой на сообщение того, кого хочешь изгнать',
            reply_to_message_id=update.effective_message.message_id
        )
        return

    banished_name = (' '.join(context.args)).lower()
    banished_names = ElderStorage.get_all_banished_names()
    banishing_user = update.message.reply_to_message.from_user

    admins = await update.effective_chat.get_administrators()

    banishing_member = None
    for member in admins:
        if member.user['id'] == banishing_user['id']:
            banishing_member = member
            break

    def _validate_banish_name(name: str) -> bool:
        return (
                not name
                or not bool(re.search(r'^[а-я ]+$', name))
                or name == consts.ELDER_ONE
                or len(name) < 4
                or len(name) > 16
        )

    jobs = context.job_queue.jobs()
    validators = {
        (
            'Изгнанный не может никого изгнать'
        ): lambda: ElderStorage.get_banishment(user_id),
        (
            'Это не старейшина'
        ): lambda: not banishing_member,
        (
            'Это создатель группы. Я не могу его изгнать'
        ): lambda: banishing_member.status == ChatMember.OWNER,
        (
            'Прямо сейчас решается судьба одного '
            'из старейшин, тебе придется подождать'
        ): lambda: _is_there_any_poll_jobs(jobs),
        (
            'Этот старейшнина уже отбывает свое изгнание'
        ): lambda: ElderStorage.get_banishment(banishing_user['id']),
        (
            'Формат команды: /banish <прозвище в изгнании на русском>'
        ): lambda: (
            _validate_banish_name(banished_name)
        ),
        (
            'Максимальное кол-во изгнанных'
        ): lambda: len(banished_names) >= consts.MAX_BANISHMENTS,
        (
            'Такой изгнанный уже есть'
        ): lambda: banished_name in banished_names,
        (
            'Дух не может быть изгнан'
        ): lambda: banishing_user['id'] == context.bot.bot['id'],
    }

    for message, validation_failed in validators.items():
        if validation_failed():
            await update.effective_chat.send_message(
                text=message,
                reply_to_message_id=update.effective_message.message_id
            )
            return

    banished_user = update.message.reply_to_message.from_user

    ElderStorage.create_or_update_last_cast_date(user_id)
    mentions = " ".join(
        f'<a href="tg://user?id={member.user["id"]}">{member.user["first_name"]}</a>'
        for member in admins
        if not member.user["is_bot"]
    )
    elder_call = f'Все старейшины слышат зов духа... {mentions}'

    await update.effective_chat.send_message(elder_call, parse_mode=ParseMode.HTML)

    poll_text = f'Время страшного суда. Отправится ли {banished_user["first_name"]} в изгнание под прозвищем "{banished_name}"?'

    poll_message = await update.effective_chat.send_poll(
        f'{poll_text}\n\nГолосование продлится {consts.POLL_DURATION_SECONDS} сек',
        ['Да', 'Нет'],
        is_anonymous=False
    )

    context.job_queue.run_once(banishment, consts.POLL_DURATION_SECONDS, data={
        'message_id': poll_message.message_id,
        'chat_id': poll_message.chat_id,
        'update': update,
        'banished_user_id': banished_user['id'],
        'banished_name': banished_name,
        'banished_first_name': banished_user['first_name'],
        'user_id': user_id,
        'chat_title': update.effective_chat['title'],
        'job_type': consts.POLL_JOB_TYPE
    })


async def banishment(context):
    job_context = context.job.data
    poll = await context.bot.stop_poll(
        job_context['chat_id'],
        job_context['message_id'],
    )

    update = job_context['update']
    banished_user_id = job_context['banished_user_id']
    new_banished_name = job_context['banished_name']
    banished_first_name = job_context['banished_first_name']
    user_id = job_context['user_id']
    old_chat_title = job_context['chat_title']

    banishment_solution = await banishment_poll_results(poll.options, update.effective_chat)
    new_banished_name = fix_banished_name(new_banished_name)

    if not banishment_solution:
        await update.effective_chat.send_message(
            text=f'Сегодня судьба благосклонна к {banished_first_name}. '
                 f'Он не {new_banished_name}, он - {consts.ELDER_ONE}...',
        )
        return

    banishment_days = randrange(1, 6)

    ElderStorage.create_or_update_banishment(
        banished_user_id,
        new_banished_name,
        banishment_days=banishment_days,
        chat_id=job_context['chat_id'],
    )
    ElderStorage.create_or_update_last_cast_date(user_id)
    # ElderStorage.create_action(
    #     banished_user_id,
    #     0,
    #     "",
    #     f'Решением совета {banished_first_name} сегодня был изгнан из лагеря на {banishment_days} дней.'
    # )

    await update.effective_chat.set_administrator_custom_title(
        banished_user_id,
        new_banished_name
    )

    new_chat_title = get_new_chat_title(old_chat_title)
    await context.bot.set_chat_title(update.effective_chat.id, new_chat_title)
    ElderStorage.set_chat_title(update.effective_chat.id, new_chat_title)

    context.job_queue.run_once(
        banishment_return,
        timedelta(days=banishment_days),
        data={
            'chat_id': update.effective_chat.id,
            'banished_user_id': banished_user_id,
            'banished_name': new_banished_name,
            'job_type': consts.BANISH_RETURN_JOB_TYPE,
            'banished_first_name': banished_first_name
        }
    )

    await update.effective_chat.send_message(
        text=f'{banished_first_name} отправляется в изгнание на {banishment_days} дней.'
             f' Отныне называйте его <b>{new_banished_name.upper()}</b>',
        parse_mode=ParseMode.HTML
    )


async def plan_returns(context):
    banishments = ElderStorage.get_all_banishments()

    for banish in banishments:
        banished_user_id = banish[0]
        banished_name = banish[1]
        banished_chat_id = banish[4]
        return_date = datetime.strptime(banish[3], "%Y-%m-%d %H:%M:%S")
        delta_return = return_date - datetime.now()
        chat = await context.bot.get_chat(banished_chat_id)
        admins = await chat.get_administrators()
        elders = {
            member.user['id']: member.user['first_name']
            for member in admins
            if not member.user['is_bot']
        }

        context.job_queue.run_once(
            banishment_return,
            delta_return,
            data={
                'chat_id': banished_chat_id,
                'banished_user_id': banished_user_id,
                'banished_name': banished_name,
                'job_type': consts.BANISH_RETURN_JOB_TYPE,
                'banished_first_name': elders.get(banished_user_id)
            }
        )
    print(context.job_queue.jobs())
    print(datetime.now(), 'planned')


async def banishment_return(context):
    job_context = context.job.data
    chat_id = job_context['chat_id']

    banished_user_id = job_context['banished_user_id']
    banished_first_name = job_context['banished_first_name']
    banished_name = job_context['banished_name']
    chat_title = ElderStorage.get_chat_title(chat_id=chat_id)

    ElderStorage.delete_banishment(banished_user_id)
    new_chat_title = get_new_chat_title(chat_title)

    await context.bot.set_chat_title(chat_id, new_chat_title)
    ElderStorage.set_chat_title(chat_id, new_chat_title)
    # ElderStorage.create_action(
    #     banished_user_id,
    #     0,
    #     "",
    #     f'{banished_first_name} отбыл своё наказание и сегодня вернулся в лагерь старейшин.'
    # )
    await (await context.bot.get_chat(job_context['chat_id'])).set_administrator_custom_title(
        banished_user_id,
        consts.ELDER_ONE
    )

    await context.bot.send_message(
        chat_id,
        text=f'<b>{banished_name.upper()}</b> смиренно отбыл изгнание и вернулся, осознав свои ошибки.'
             f' Теперь он снова просто старейшина. Один из многих...',
        parse_mode=ParseMode.HTML
    )


async def delete_message(context):
    job_context = context.job.data
    chat_id = job_context['chat_id']
    message_id = job_context['message_id']
    await context.bot.delete_message(
        chat_id=chat_id, message_id=message_id
    )


def _get_summary_messages(base_context, last_actions):
    return [{
        "role": "system",
        "content": base_context
    }, {
        "role": "user",
        "content": f"За сегодняшний день произошли следующие события: {last_actions}"
    }, {
        "role": "user",
        "content": f"Сделай короткий и захватывающий рассказ подводящий итоги этого дня. Максимум 6 предложений."
    }]


async def day_summary(context):
    chat = await context.bot.get_chat(consts.ELDERS_CHAT_ID)
    admins = await chat.get_administrators()
    elders = {
        member.user['id']: member.user['first_name']
        for member in admins
        if not member.user['is_bot']
    }

    base_context = get_base_context(elders)
    today_actions = [action[3] for action in ElderStorage.get_today_actions()]

    if not today_actions:
        return

    ai_action_result = await _get_ai_response(_get_summary_messages(" ".join(today_actions), base_context), 0.7)

    n_action = 4
    while not ai_action_result and n_action > 1:
        last_actions_cut = [
            action
            for idx, action in enumerate(today_actions)
            if idx % n_action != 0
        ]
        ai_action_result = await _get_ai_response(_get_summary_messages(" ".join(last_actions_cut), base_context), 0.7)
        n_action -= 1
    await context.bot.send_message(
        chat_id,
        text=f'<b>Итоги дня</b>\n\n{ai_action_result}',
        parse_mode=ParseMode.HTML
    )


async def clean_old_actions(context):
    ElderStorage.clean_old_actions()
