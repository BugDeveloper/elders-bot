import functools
import sqlite3
from datetime import datetime, timedelta

import consts
from storage.sql_statements import *


def connection(func):
    @functools.wraps(func)
    def decorator(cls, *args, **kwargs):
        conn = cls._get_connection()
        return func(cls, *args, **kwargs, conn=conn)
    return decorator


class ElderStorage:
    _conn = None

    @classmethod
    @connection
    def delete_banishment(cls, user_id, conn):
        cur = conn.cursor()
        cur.execute(DELETE_BANISHMENT.format(user_id=user_id))
        conn.commit()

    @classmethod
    @connection
    def get_chat_title(cls, chat_id, conn):
        cur = conn.cursor()
        cur.execute(GET_CHAT_TITLE.format(chat_id=chat_id))
        return cur.fetchone()[0]

    @classmethod
    @connection
    def set_chat_title(cls, chat_id, chat_title, conn):
        cur = conn.cursor()
        cur.execute(INSERT_OR_REPLACE_CHAT_TITLE.format(chat_id=chat_id, chat_title=chat_title))
        conn.commit()

    @classmethod
    @connection
    def get_banishment_end_date(cls, user_id, conn):
        cur = conn.cursor()
        cur.execute(GET_BANISHMENT_TERMS.format(user_id=user_id))
        banishment = cur.fetchone()
        if not banishment:
            return
        return banishment[0], datetime.strptime(banishment[1], '%Y-%m-%d %H:%M:%S')

    @classmethod
    @connection
    def get_banishment(cls, user_id, conn):
        cur = conn.cursor()
        cur.execute(GET_BANISHMENT.format(user_id=user_id))
        banishment = cur.fetchone()
        if not banishment:
            return

        return datetime.strptime(banishment[2], '%Y-%m-%d %H:%M:%S')

    @classmethod
    @connection
    def get_all_banished_names(cls, conn):
        cur = conn.cursor()
        cur.execute(GET_ALL_BANISHED_NAMES)
        banished_names = cur.fetchall()
        return [result[0] for result in banished_names]

    @classmethod
    @connection
    def get_all_banishments(cls, conn):
        cur = conn.cursor()
        cur.execute(GET_ALL_BANISHMENTS)
        return cur.fetchall()

    @classmethod
    @connection
    def create_ai_learning(cls, initial_action, action_text, conn):
        cur = conn.cursor()
        cur.execute(INSERT_AI_LEARN_RECORD.format(
            initial_action=initial_action,
            action_text=action_text,
        ))
        conn.commit()

    @classmethod
    @connection
    def create_action(cls, user_id, message_id, initial_action, action_text, conn):
        cur = conn.cursor()
        action_date = datetime.now()

        cur.execute(INSERT_ACTION_RECORD.format(
            user_id=user_id,
            message_id=message_id,
            initial_action=initial_action,
            action_text=action_text,
            action_date=action_date,
        ))
        conn.commit()

    @classmethod
    @connection
    def get_action_by_message_id(cls, message_id, conn):
        cur = conn.cursor()
        cur.execute(GET_ACTION_BY_MESSAGE_ID.format(message_id=message_id))
        return cur.fetchone()

    @classmethod
    @connection
    def get_today_actions(cls, conn):
        cur = conn.cursor()
        cur.execute(GET_TODAY_ACTIONS)
        return cur.fetchall()

    @classmethod
    @connection
    def clean_old_actions(cls, conn):
        cur = conn.cursor()
        cur.execute(CLEAN_OLD_ACTIONS)
        conn.commit()

    @classmethod
    @connection
    def create_or_update_banishment(cls, user_id, banished_name, banishment_days, chat_id, conn):
        cur = conn.cursor()
        banished_date = datetime.now()
        cur.execute(INSERT_OR_REPLACE_BANISHMENT_RECORD.format(
            user_id=user_id,
            banished_name=banished_name,
            banished_date=banished_date,
            banishment_end_date=banished_date + timedelta(days=banishment_days),
            chat_id=chat_id
        ))
        conn.commit()

    @classmethod
    @connection
    def get_last_cast_date(cls, user_id, conn):
        cur = conn.cursor()
        cur.execute(GET_LAST_CAST.format(user_id=user_id))
        last_cast = cur.fetchone()
        if not last_cast:
            return
        return datetime.strptime(last_cast[1], '%Y-%m-%d %H:%M:%S')

    @classmethod
    @connection
    def create_or_update_last_cast_date(cls, user_id, conn):
        cur = conn.cursor()
        cur.execute(INSERT_OR_REPLACE_BANISH_CAST.format(user_id=user_id, cast_date=datetime.now()))
        conn.commit()

    @classmethod
    @connection
    def migrate(cls, conn):
        cur = conn.cursor()
        cur.execute(CREATE_BANISHMENT_TABLE)
        cur.execute(CREATE_BANISH_CAST_TABLE)
        cur.execute(CREATE_CHAT_DATA_TABLE)
        cur.execute(CREATE_AI_CONTEXT_TABLE)
        cur.execute(CREATE_LEARNING_TABLE)
        conn.commit()

    @classmethod
    def _get_connection(cls):
        if not cls._conn:
            cls._conn = sqlite3.connect(consts.DB_FILE, check_same_thread=False)
        return cls._conn
