BANISHMENT_TABLE_NAME = 'banishment'
CAST_TABLE_NAME = 'banish_casted'
CHAT_DATA_TABLE_NAME = 'chat_data'
AI_CHAT_ACTIONS_TABLE_NAME = 'ai_chat_actions'
AI_LEARNING_TABLE_NAME = 'ai_learning'

CREATE_BANISHMENT_TABLE = """ 
    CREATE TABLE IF NOT EXISTS """ + BANISHMENT_TABLE_NAME + """ (
        user_id integer PRIMARY KEY,
        banished_name text,
        banished_date text,
        banishment_end_date text,
        chat_id integer
    ); 
"""

CREATE_BANISH_CAST_TABLE = """ 
    CREATE TABLE IF NOT EXISTS """ + CAST_TABLE_NAME + """ (
        user_id integer PRIMARY KEY,
        cast_date text
    ); 
"""

CREATE_CHAT_DATA_TABLE = """ 
    CREATE TABLE IF NOT EXISTS """ + CHAT_DATA_TABLE_NAME + """ (
        chat_id integer PRIMARY KEY,
        chat_title text
    ); 
"""

CREATE_AI_CONTEXT_TABLE = """
    CREATE TABLE IF NOT EXISTS """ + AI_CHAT_ACTIONS_TABLE_NAME + """ (
        id integer PRIMARY KEY AUTOINCREMENT,
        user_id integer,
        message_id integer,
        initial_action text,
        action_text text,
        action_date text
    ); 
"""

CREATE_LEARNING_TABLE = """
    CREATE TABLE IF NOT EXISTS """ + AI_LEARNING_TABLE_NAME + """ (
        id integer PRIMARY KEY AUTOINCREMENT,
        initial_action text,
        action_text text
    ); 
"""

GET_CHAT_TITLE = 'SELECT chat_title FROM ' + CHAT_DATA_TABLE_NAME + ' WHERE chat_id = {chat_id} LIMIT 1;'
INSERT_OR_REPLACE_CHAT_TITLE = (
        'INSERT OR REPLACE INTO ' + CHAT_DATA_TABLE_NAME + ' (chat_id, chat_title) VALUES ({chat_id}, "{chat_title}");'
)

GET_TODAY_ACTIONS = 'SELECT * FROM ' + AI_CHAT_ACTIONS_TABLE_NAME + ' WHERE Datetime(action_date) > Datetime("now", "-1 day");'

GET_LAST_CAST = 'SELECT * FROM ' + CAST_TABLE_NAME + ' WHERE user_id = {user_id} LIMIT 1;'
GET_BANISHMENT = 'SELECT * FROM ' + BANISHMENT_TABLE_NAME + ' WHERE user_id = {user_id} LIMIT 1;'
GET_BANISHMENT_TERMS = 'SELECT banished_name, banishment_end_date FROM ' + BANISHMENT_TABLE_NAME + ' WHERE user_id = {user_id} LIMIT 1;'
GET_ALL_BANISHED_NAMES = 'SELECT banished_name FROM ' + BANISHMENT_TABLE_NAME + ';'
GET_ALL_BANISHMENTS = 'SELECT * FROM ' + BANISHMENT_TABLE_NAME + ';'

DELETE_BANISHMENT = 'DELETE FROM ' + BANISHMENT_TABLE_NAME + ' WHERE user_id = {user_id};'
CLEAN_OLD_ACTIONS = 'DELETE FROM ' + AI_CHAT_ACTIONS_TABLE_NAME + ' WHERE Datetime(action_date) < Datetime("now", "-2 days");'

INSERT_OR_REPLACE_BANISH_CAST = (
        'INSERT OR REPLACE INTO ' + CAST_TABLE_NAME + ' (user_id, cast_date) VALUES '
                                                      '({user_id}, datetime("{cast_date}"));'
)

INSERT_OR_REPLACE_BANISHMENT_RECORD = (
        'INSERT OR REPLACE INTO ' + BANISHMENT_TABLE_NAME +
        ' (user_id, banished_name, banished_date, banishment_end_date, chat_id) VALUES ({user_id}, "{banished_name}",'
        ' datetime("{banished_date}"), datetime("{banishment_end_date}"), {chat_id});'
)

INSERT_ACTION_RECORD = (
        'INSERT INTO ' + AI_CHAT_ACTIONS_TABLE_NAME +
        ' (user_id, message_id, initial_action, action_text, action_date) VALUES ({user_id}, {message_id}, "{initial_action}", "{action_text}",  datetime("{action_date}"));'
)

INSERT_AI_LEARN_RECORD = (
        'INSERT INTO ' + AI_LEARNING_TABLE_NAME +
        ' (initial_action, action_text) VALUES ("{initial_action}", "{action_text}");'
)

GET_ACTION_BY_MESSAGE_ID = (
        'SELECT initial_action, action_text FROM ' + AI_CHAT_ACTIONS_TABLE_NAME +
        ' WHERE message_id = {message_id} LIMIT 1;'
)
