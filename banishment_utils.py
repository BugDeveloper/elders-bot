import random
import re
import string

from telegram.constants import ParseMode
from storage.storage import ElderStorage


async def banishment_poll_results(poll_options, chat):
    yes_option, no_option = poll_options
    yes_count = yes_option['voter_count']
    no_count = no_option['voter_count']

    if yes_count == no_count:
        await chat.send_message(
            text=f'Голосование показало раскол старейшин. Дух решит сам...',
            parse_mode=ParseMode.HTML
        )
        return random.choice([True, False])

    return yes_count > no_count


def get_new_chat_title(old_chat_title):
    banished_names = ElderStorage.get_all_banished_names()
    big_number = 9999
    comma_idx = old_chat_title.find(', ')
    and_idx = old_chat_title.find(' и ')

    comma_idx = comma_idx if comma_idx != -1 else big_number
    and_idx = and_idx if and_idx != -1 else big_number

    first_delimiter_idx = min(comma_idx, and_idx)

    new_chat_title = f'{old_chat_title[:first_delimiter_idx]}'
    comma_separated_names = ", ".join(banished_names[:-1])

    if banished_names:
        if comma_separated_names:
            new_chat_title = f'{new_chat_title}, {comma_separated_names}'
        new_chat_title = re.sub(' +', ' ', f'{new_chat_title} и {banished_names[-1]}')
    return new_chat_title


def fix_banished_name(new_banished_name):
    return new_banished_name.translate(str.maketrans('', '', string.punctuation))
