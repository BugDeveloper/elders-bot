import functools

from telegram.constants import ChatType

import consts


def send_action(action):
    def decorator(func):
        @functools.wraps(func)
        async def command_func(update, context, *args, **kwargs):
            await context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=action)
            return await func(update, context, *args, **kwargs)

        return command_func

    return decorator


def only_group(func):
    @functools.wraps(func)
    async def decorator(update, context):
        if update.effective_chat['type'] != ChatType.SUPERGROUP:
            await context.bot.send_message(
                chat_id=update.message.chat_id,
                text="Это не группа старейшин 😡"
            )
            return
        return await func(update, context)

    return decorator
