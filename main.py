import os
from datetime import datetime, time

import pytz
from telegram import Bot
from telegram.ext import Application, CommandHandler, AIORateLimiter, MessageHandler, filters
import handlers
from storage.storage import ElderStorage

TOKEN = os.environ['TOKEN']

ElderStorage.migrate()

# Initialize the bot and the application with AIO-based rate limiter
bot = Bot(token=TOKEN)
application = Application.builder().token(TOKEN).rate_limiter(AIORateLimiter()).build()

# Handlers
make_elder_handler = CommandHandler('make_elder_one', handlers.make_elder)
banish_handler = CommandHandler('banish', handlers.banishment_poll)
banishment_term_handler = CommandHandler('term', handlers.banishment_term)
action_handler = CommandHandler('action', handlers.elder_action)
remember_handler = CommandHandler('remember', handlers.remember_action)

message_handler = MessageHandler(filters.ALL & ~filters.COMMAND, handlers.react_to_banished)


# Add handlers to the application
application.add_handler(make_elder_handler)
application.add_handler(banish_handler)
application.add_handler(banishment_term_handler)
application.add_handler(action_handler)
application.add_handler(remember_handler)
application.add_handler(message_handler)

# Error handler
application.add_error_handler(handlers.error_handler)

# Jobs (equivalent to the old job_queue)
application.job_queue.run_once(handlers.plan_returns, 3)
application.job_queue.run_daily(handlers.clean_old_actions, datetime.now())
application.job_queue.run_daily(handlers.day_summary, time(21, 0, 0, 0, pytz.timezone('Asia/Istanbul')))

# Start the bot
application.run_polling()
